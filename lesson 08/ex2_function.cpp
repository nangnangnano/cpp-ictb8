#include<iostream>
using namespace std;

float FindMax(float a,float b=0.0){
	return (a>b)?a:b;
}

int main(){
	float n1,n2;
	cout<<"Max of 8 and 9 is: "<<FindMax(8)<<endl;
	cout<<"Enter N1:"; cin>>n1;
	cout<<"Enter N2:"; cin>>n2;
	
	cout<<"Max of "<<n1<<" and "<<n2<<" is :"<<FindMax(n1,n2)<<endl;
	
	return(0);
}
